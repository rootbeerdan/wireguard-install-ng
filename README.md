# WireGuard installer

**This project is a bash script that aims to setup a [WireGuard](https://www.wireguard.com/) VPN on a Linux server, as easily as possible!**

**This project is forked from https://github.com/angristan/wireguard-install**

WireGuard is a point-to-point VPN that can be used in different ways. Here, we mean a VPN as in: the client will forward all its traffic trough an encrypted tunnel to the server.
The server will apply NAT to the client's traffic so it will appear as if the client is browsing the web with the server's IP.

The script supports both IPv4 and IPv6. Please check the [issues](https://gitlab.com/rootbeerdan/wireguard-install-ng/-/issues) for ongoing development, bugs and planned features!

## Requirements

Supported distributions (Others may work, these are the only distros being actively tested):

- Ubuntu 18.04
- Ubuntu 20.04
- Amazon Linux 2

## Usage

Download and execute the script. Answer the questions asked by the script and it will take care of the rest.

```bash
curl -O https://gitlab.com/rootbeerdan/wireguard-install-ng/-/raw/master/wireguard-install.sh
chmod +x wireguard-install.sh
./wireguard-install.sh
```

It will install WireGuard (kernel module and tools) on the server, configure it, create a systemd service and a client configuration file.

Run the script again to add or remove clients!

## Notes

### IPv6 Support

Most operating systems will **NOT** prioritize IPv6 routes (aka Happy Eyeballs) that are part of the ULA IPv6 range (fc00::/7). This means that if you use the default IP given to you in this script, you will default to IPv4.

While you could just put in any other /64 availible on the internet (just like you could use any /24 outside of RFC1918), it is best practice to use only the IPv6 space you own to avoid conflicts. If you want IP space that won't be used by anyone else, I would suggest using AWS and getting your own IPv6 subnet delegated to a VPC. This ensures you will not overlap with anyone else.
